﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio2
{
    class Carro
    {
        public string color;
        public string marca;
        public int anyoModelo;
        public int numeroPuertas;
        public decimal precio;
        public Tanque miTanque;
        public Motor miMotor;

        public Carro()
        {
            miTanque = new Tanque();
            miMotor = new Motor();
        }

        public void acelerar()
        {
            Console.WriteLine("Estoy acelerando");
            miMotor.consumirCombustible(ref miTanque, (decimal)0.4);
            Console.WriteLine("Remanente de combustible: " + miTanque.obtenerLitros().ToString());
        }

        public void frenar()
        {
            Console.WriteLine("Estoy frenando");
        }

        public void obtenerCaracteristicas()
        {
            Console.WriteLine("Caracteristicas del auto");
            Console.WriteLine("Color: " + color);
            Console.WriteLine("Marca: " + marca); 
            Console.WriteLine("Año del modelo: " + anyoModelo.ToString());
            Console.WriteLine("Número de puertas: " + numeroPuertas);
            Console.WriteLine("Precio: " + precio.ToString());
            Console.WriteLine("Tipo de carburador: " + miMotor.obtenerTipoCarburador());
            Console.WriteLine("Número de cilindros: " + miMotor.obtenerNumeroCilindros().ToString());
            Console.WriteLine("Tipo de combustible: " + miMotor.TipoCombustible);
        }
    }
}
