﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio2
{
    class Tanque
    {
        private decimal cantidadLitrosCombustible;
        const decimal MAXIMO_LITROS = 40;

        public Tanque()
        {
            cantidadLitrosCombustible = 0;
        }

        public void llenarTanque(decimal cantidadCombustible)
        {
            if (cantidadCombustible > 0)
            {
                if ((cantidadCombustible + cantidadLitrosCombustible) <= MAXIMO_LITROS)
                {
                    cantidadLitrosCombustible += cantidadCombustible;
                    Console.WriteLine("Total de combustible: " + cantidadLitrosCombustible.ToString());
                } else
                {
                    cantidadLitrosCombustible = MAXIMO_LITROS;
                    Console.WriteLine("Tanque lleno!");
                }
            }
        }

        public void vaciarTanque(decimal cantidadCombustible)
        {
            if (cantidadCombustible <= cantidadLitrosCombustible)
            {
                cantidadLitrosCombustible -= cantidadCombustible;
                Console.WriteLine("Total de combustible: " + cantidadLitrosCombustible.ToString());
            } else
            {
                cantidadLitrosCombustible = 0;
                Console.WriteLine("Tanque vacio!");
            }
        }

        public decimal obtenerLitros()
        {
            return cantidadLitrosCombustible;
        }
    }
}
